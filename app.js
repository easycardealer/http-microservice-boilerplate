const express = require('express');
const fs = require('fs');
const helmet = require('helmet');
const config = require('config');
const routes = require('./src/routes');

const app = express();
const port = config.get('port');

app.use(helmet())
app.use(routes);

const packageJson = JSON.parse(fs.readFileSync('./package.json').toString());

app.listen(port, (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log(`Started ${packageJson.name} successfully on port ${port}`);
});
