# HTTP-Microservice-Boilerplate

Use this boilerplate code in order to setup an autengo HTTP microservice.
Locate your routes in the corresponding version folder "/v1/" and create an own directory for your route like for an example route you will create "/v1/example/example.route.js"

If not needed the following services can be deleted: 

- helmet
- db.js

Delete the 'example' extension in local.yaml.example and place your config stuff there.

