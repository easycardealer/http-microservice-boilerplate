const express = require('express');
const bodyParser = require('body-parser');

const routesV1 = require('./v1/index');
const logger = require('../services/logger');
const errorHandler = require('../services/errorService').errorHandler;

const router = express.Router();
router.use(bodyParser.json({ limit: '50mb' }));

router.use((req, res, next) => {
    logger.info(`${req.method}: ${req.url}`, { url: req.url, method: req.method });
    return next();
});

router.use('/v1/', routesV1);
router.use(errorHandler);

module.exports = router;
