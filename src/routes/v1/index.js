const express = require('express');
const exampleRoute = require('./example/example.router');

const router = express.Router();

router.use('/example', exampleRoute);

module.exports = router;
