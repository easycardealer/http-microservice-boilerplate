const router = require('express').Router();
const ExampleController = require('./example.controller');
const errors = require('../../../services/errorService').errors;

router.route('/')
    .get(ExampleController.getExample)
    .all((req, res, next) => {
        return next(errors.notAllowedError);
    });

module.exports = router;
