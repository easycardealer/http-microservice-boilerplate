async function getExample(req, res, next) {
    try {
        // process some stuff
        res.send({
            data: {}
        })
    } catch (err) {
        next(err);
    }
}

module.exports = { getExample };
