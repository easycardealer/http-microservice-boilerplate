const mysql = require('mysql');
const config = require('config');

const pool = mysql.createPool({
    connectionLimit: 10,
    host: config.get('mysql.host'),
    user: config.get('mysql.user'),
    password: config.get('mysql.password'),
    database: config.get('mysql.database'),
    timezone: 'utc',
});

const escapeId = function (str) {
    return pool.escapeId(str);
};

const escape = function (str) {
    return pool.escape(str);
};

const query = function (queryString, parameters, connection = null) {
    return new Promise((resolve, reject) => {
        const resultCb = (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        };

        if (connection) {
            connection.query(queryString, parameters, resultCb);
        } else {
            pool.query(queryString, parameters, resultCb);
        }
    });
};


const beginTransaction = function () {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }

            return connection.beginTransaction((transactionError) => {
                if (transactionError) {
                    return reject(transactionError);
                }
                return resolve(connection);
            });
        });
    });
};

const rollbackConnection = function (connection) {
    if (!connection) {
        return Promise.reject(new Error('No connection'));
    }

    return new Promise((resolve) => {
        connection.rollback(() => {
            resolve();
        });
    });
};

const commitConnection = function (connection) {
    if (!connection) {
        return Promise.reject(new Error('No connection'));
    }

    return new Promise((resolve, reject) => {
        connection.commit((err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

const getConnection = function () {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                resolve(connection);
            }
        });
    });
};

const getFoundRows = function (connection) {
    return new Promise((resolve, reject) => {
        query('SELECT FOUND_ROWS() as count', null, connection)
            .then(results => {
                if (!results || results.length === 0) {
                    resolve(0);
                }
                resolve(results[0].count)
            })
            .catch(err => reject(err))
    });
};

async function runTransaction(transactionFn) {
    let connection;

    try {
        connection = await beginTransaction();
    } catch (error) {
        error.subject('Failed to begin transaction');
        throw error;
    }

    try {
        const result = await transactionFn(connection);
        await commitConnection(connection);
        await connection.release();
        return result;
    } catch (error) {
        if (connection) {
            try {
                await rollbackConnection(connection);
                await connection.release();
            } catch (rollbackError) {
                throw rollbackError;
            }
        }

        throw error;
    }
}

module.exports = {
    query,
    beginTransaction,
    rollbackConnection,
    commitConnection,
    getConnection,
    escapeId,
    escape,
    getFoundRows,
    runTransaction,
};
