const logger = require('./logger');

const errorHandler = (err, req, res, next) => {
    const status = err.status || 500;
    const body = {
        error: err.message,
        status,
    };

    logger.error(err);

    return res.status(status).send(body);
};

const notAllowedError = new Error('NotAllowed');
notAllowedError.status = 405;

module.exports = {
    errorHandler,
    errors: {
        notAllowedError,
    },
};
