const util = require('util');
const { createLogger, format, transports } = require('winston');
const { LEVEL, MESSAGE, SPLAT } = require('triple-beam');
const config = require('config');
const Elasticsearch = require('winston-elasticsearch');

format.prettierPrint = format((log, opts) => {
    const level = log[LEVEL] || log.level;
    const message = log[MESSAGE] || log.message;
    const splat = log[SPLAT] || log.splat;
    const { timestamp, stack } = log;

    log[MESSAGE] = `${timestamp} ${log.level.replace(level, level.toUpperCase())}: ${stack || message}${
        splat && splat.length
            ? splat.map(obj => '\n' + util.inspect(obj, true, 1, opts.colors)).join('')
            : ''
    }`;
    return log;
});

const logger = new createLogger({
    levels: {
        trace: 9,
        input: 8,
        verbose: 7,
        prompt: 6,
        debug: 5,
        info: 4,
        data: 3,
        help: 2,
        warn: 1,
        error: 0,
    },
    format: format.combine(
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      format.errors({ stack: true }),
    ),
});

logger.add(new transports.Console({
    level: config.has('log.level') ? config.get('log.level') : 'info',
    format: format.combine(
        format.colorize(),
        format.prettierPrint({ colors: true }),
    )
}));

if (config.has('log.file')) {
    logger.add(new transports.File({
        level: config.has('log.level') ? config.get('log.level') : 'info',
        filename: config.get('log.file'),
        format: format.prettierPrint(),
    }));
}

if (config.has('elastic.endpoint')) {
    let elasticOpts = {
        clientOpts: {
            hosts: [
                config.get('elastic.endpoint'),
            ],
        },
        indexPrefix: `logs-${config.get('elastic.indexPrefix')}`,
        level: 'error',
        mappingTemplate: './resources/index-mapping.json',
    };
    logger.info('Using elastic logging');
    logger.add(new Elasticsearch(elasticOpts));
}

module.exports = logger;
